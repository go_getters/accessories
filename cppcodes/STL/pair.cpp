#include <iostream> 
#include <utility> 
using namespace std; 
  
int main() 
{ 
    pair <int, string> PAIR1 ; 
  
    PAIR1.first = 100; 
    PAIR1.second = 'G' ; 
  
    std::cout << PAIR1.first << " " ; 
    std::cout << PAIR1.second << endl ; 

    /////////////////////////////////////////
    pair <int,char> g2 = make_pair(1, 'a');

    cout<<g2.first<<" ";
    cout<<g2.second<<" "<<endl;
    ///////////////////////////////////////
    pair <string,double> PAIR2 ("GeeksForGeeks", 1.23); 
  
    cout << PAIR2.first << " " ; 
    cout << PAIR2.second << endl ; 
    ////////////////////////////////////////
    ///Swapping of two pairs
    pair<string, int>pair1 = make_pair("hello", 1); 
    pair<string, int>pair2 = make_pair("A", 2); 
  
    cout << "Before swapping:\n " ; 
    cout << "Contents of pair1 = " << pair1.first << " " << pair1.second ; 
    cout << "Contents of pair2 = " << pair2.first << " " << pair2.second ; 
    cout << endl << "Pair Addition" << endl;
    cout << pair1.first + pair2.first <<endl;
    cout << pair1.second + pair2.second <<endl;

    swap(pair1,pair2);
  
    cout << "\nAfter swapping:\n "; 
    cout << "Contents of pair1 = " << pair1.first << " " << pair1.second ; 
    cout << "Contents of pair2 = " << pair2.first << " " << pair2.second ; 
    return 0; 
} 
#include <vector>
#include<iostream>
using namespace std; 
  
int main() 
{ 
    // initialising the vector 
    // vector<int> vec = { 10, 20, 30, 40 }; 
  
    // // inserts 3 at front 
    // auto it = vec.insert(vec.begin(), 3); 
    // // inserts 2 at front 
    // vec.insert(it, 2); 
  
    // int i = 2; 
    // // inserts 7 at i-th index 
    // it = vec.insert(vec.begin() + 5, 7); 
    std::vector<int> v,v2;
    for(int i=0;i<17;i++)
    {   
        v.push_back(i+100);
    }
    for(int i=0;i<17;i++)
    {   
        v2.push_back(i+10);
    }
    std::cout<<v.at(2)<<" : "<<v.front()<<" : "<<v.back()<<" : "<<*((int*)(v.data()))<<std::endl;

    v.insert(v.begin()+5,5);

    std::cout << "The vector elements are: "; 
    for (auto it = v.begin(); it != v.end(); ++it) 
        cout << *it << " "; 

        
    //swaping the vectors
    swap(v,v2);
    for (auto it = v2.begin(); it != v2.end(); ++it) 
        cout << *it << " "; 
    cout<<endl;
    for (auto it = v.begin(); it != v.end(); ++it) 
        cout << *it << " "; 

    return 0; 
} 
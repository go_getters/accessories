#include<iostream>
#define BOOST_CHRONO_VERSION 2
#include<boost/chrono.hpp>
#include<typeinfo>
#include <sstream> 
#include<string>
//COMPILE:  g++ exchrono.cpp -lboost_system -lboost_chrono
int main()
{	namespace chrono=boost::chrono;
	//std::cout << chrono::system_clock::now() <<std::endl;//system time
	//std::cout << chrono::steady_clock::now() <<std::endl;//time since last boot
	std::stringstream temp1,temp2;
	temp1<<chrono::time_fmt(boost::chrono::timezone::utc, "%Y:%m:%d")<<chrono::system_clock::now();
	//Add %y for year in yy format, %D for date in yyyy:mm::dd format
	temp2<<chrono::time_fmt(boost::chrono::timezone::utc, "%H:%M:%S")<<chrono::system_clock::now();
	std::string datestr,timestr;
	temp1>>datestr;
	temp2>>timestr;
	std::cout<<datestr<<"_"<<timestr<<std::endl;
	return 0;
}
